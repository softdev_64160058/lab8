/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sarunpat.databaseproject;

import com.sarunpat.databaseproject.dao.UserDao;
import com.sarunpat.databaseproject.helper.DatabaseHelper;
import com.sarunpat.databaseproject.model.User;

/**
 *
 * @author USER
 */
public class TestUserDao {

    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        for (User u : userDao.getAll()) {
            System.out.println(u);
        }
//        User user1 = userDao.get(2);
//        System.out.println(user1);

//        User newUser = new User("user3","password","F",2);
//        User insertedUser = userDao.save(newUser);
//        System.out.println(insertedUser);
//        insertedUser.setGender("M");
//        user1.setGender("F");
//        userDao.update(user1);
//        User updateUser = userDao.get(user1.getId());
//        System.out.println(updateUser);
//        userDao.delete(user1);
        for (User u : userDao.getAll()) {
            System.out.println(u);
        }
        
        for (User u : userDao.getAll("user_name LIKE 'u%' ","user_name ASC,user_gender DESC")) {
            System.out.println(u);
        }
        
        DatabaseHelper.close();
    }
}
